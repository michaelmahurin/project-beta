from .models import Salesperson, Sale, Customer, AutomobileVO
from common.json import ModelEncoder

class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href", "id"]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "salesperson",
        "automobile",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder()
    }
