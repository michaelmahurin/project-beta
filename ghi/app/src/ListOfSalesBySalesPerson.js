import React from "react";
class ListofSalesBySalesPerson extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salespersons: [],
        }
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    }

    async handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ salesperson: value })
        this.componentDidMount();
    }
    async componentDidMount() {
        const SalespersonURL = 'http://localhost:8090/api/salespersons/';
        const response = await fetch(SalespersonURL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ "salespersons": data });
        };
        if (this.state.salesperson) {
            const salesURL = `http://localhost:8090/api/salespersons/${this.state.salesperson}/`;
            const salesResponse = await fetch(salesURL);
            if (salesResponse.ok) {
                const salesData = await salesResponse.json();
                this.setState({ "sales": salesData.sales })
            }
        }
    }

    render() {
        let tableClasses = "table table-striped d-none";
        if (this.state.salesperson) {
            tableClasses = "table table-striped";
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Sales Records by Salesperson</h1>
                        <form>
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="salesperson"
                                    required
                                    id="salesperson"
                                    value={this.state.salesperson}
                                    onChange={this.handleSalesPersonChange}
                                >
                                    <option>Choose a Salesperson</option>
                                    {this.state.salespersons?.map(salesperson => {
                                        return (
                                            <option
                                                key={salesperson.id}
                                                value={salesperson.id}
                                                >
                                            {salesperson.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                        </form>
                        <table className={tableClasses}>
                            <thead>
                                <tr>
                                    <th>Salesperson</th>
                                    <th>Automobile</th>
                                    <th>Customer</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.sales?.map(sale => {
                                    return (
                                        <tr key={sale.automobile.id}>
                                            <td>{sale.salesperson.name}</td>
                                            <td>{sale.automobile.vin}</td>
                                            <td>{sale.customer.name}</td>
                                            <td>{sale.price}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default ListofSalesBySalesPerson;
