import React from "react";

class AddASalesPersonForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: "",
            employee_number: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        const SalespersonURL = "http://localhost:8090/api/salespersons/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(SalespersonURL, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            const cleared = {
                name: "",
                employee_number: "",
            };
            this.setState(cleared);
        }
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value })
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new salesperson</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-salesperson-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.handleNameChange}
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="employee_number"
                                    id="employee_number"
                                    placeholder="employee_number"
                                    required
                                    value={this.state.employee_number}
                                    onChange={this.handleEmployeeNumberChange}
                                />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default AddASalesPersonForm;
