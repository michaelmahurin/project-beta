import React from "react";

class ListOfAllSales extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: [],
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ "sales": data.sales });
        };

    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Salesperson Employee Number</th>
                        <th>Automobile</th>
                        <th>Customer</th>
                        <th>Sales Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales.map((sale) => {
                        return (
                            <tr key={sale.automobile.vin}>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.salesperson.employee_number}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}

export default ListOfAllSales;
