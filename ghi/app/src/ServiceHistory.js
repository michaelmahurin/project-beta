import React from 'react';

class SearchList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchAppointments: [],
            automobiles: [],
            selectedVin:'',
        };
    this.handleSearch = this.handleSearch.bind(this)
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    }

    handleAutomobileChange(e) {
        
        const value = e.target.value;
        // console.log("==============",value)
        this.setState({selectedVin: value})
    }

    async handleSearch(event) {
        const value = this.state.selectedVin;
        // console.log("====this.state.selectedVin is : ", this.state.selectedVin)
        const searchUrl = `http://localhost:8080/api/vin/appointment/${value}/`;
        const searchResponse = await fetch(searchUrl);
        if(searchResponse.ok){
            const searchData = await searchResponse.json();
            this.setState({searchAppointments: searchData});
        }
    }

    async componentDidMount() {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const appointmentResponse = await fetch(appointmentUrl)
        const autoResponse = await fetch(autoUrl);
        if (appointmentResponse.ok){
            const appointmentData = await appointmentResponse.json()
            const autoData = await autoResponse.json();
            // console.log("========autoData:========",autoData)
            this.setState({appointments: appointmentData})
            this.setState({automobiles: autoData.autos})
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-1 mt-0 text-center">
                    <h1 className="display-5 fw-bold">Service Appointment History</h1>
                </div>

                <div className="container">
                    <div className="row height d-flex justify-content-center align-items-center">
                        <div className="col-md-6">




                            {/* <div className="form"> */}
                            {/* <form onSubmit={this.handleSearch} type="form" method="GET"> */}
                                <div className="input-group mb-3">
                                <select onChange={this.handleAutomobileChange} value={this.state.automobile} required id="automobile" name="automobile" className="form-select">
                                        <option value="">VIN #</option>
                                        {this.state.automobiles.map(automobile => {
                                            return (
                                                <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                            );
                                        })}
                                    </select>
                                   
                                    {/* <input onChange={this.handleSearch} type="text" className="form-control form-input" placeholder="Search by VIN #" id="searchBar"/> */}
                                    <button onClick={this.handleSearch} className="btn btn-primary" id="searchBtn" type="submit">Search</button>
                                </div>
                            {/* </form> */}
                            {/* </div> */}



{/*    
Original Code by EricL                         
<div className="form-floating mb-3">
    <input onChange={this.handleSearch} type="text" className="form-control form-input my-5" placeholder="Search by VIN #" id="searchBar"/>
    <button className="btn btn-primary" id="searchBtn" type="submit">Search</button>
</div> */}
                        </div>
                    </div>
                </div>

            <table className="table table-striped my-5">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.searchAppointments.map(appoint => {
                        return (
                        <tr key={appoint.id}>
                            <td id="appointData">{ appoint.automobile.vin }</td>
                            <td id="appointData">{ appoint.owner }</td>
                            <td id="appointData">{ appoint.date }</td>
                            <td id="appointData">{ appoint.time }</td>
                            <td id="appointData">{ appoint.tech.name }</td>
                            <td id="appointData">{ appoint.reason }</td>
                        </tr>
                        );
                    })}  
                </tbody>
            </table>
    </>
    );
  }
}

export default SearchList;


