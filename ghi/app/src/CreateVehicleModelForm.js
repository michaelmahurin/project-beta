import React from "react";

class CreateVehicleModelForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: "",
            picture_url: "",
            manufacturers: [],
            manufacturer_id: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePictureURLChange = this.handlePictureURLChange.bind(this);
        this.handleManufacturerIDChange = this.handleManufacturerIDChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;
        const modelsURL = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(modelsURL, fetchConfig);
        if (response.ok) {

            const cleared = {
                name: "",
                picture_url: "",
                manufacturer: "",
            };
            this.setState(cleared);
        }
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handlePictureURLChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value })
    }
    handleManufacturerIDChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer_id: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ "manufacturers": data.manufacturers });
        };
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new vehicle model</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-model-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.handleNameChange}
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="picture_url"
                                    id="picture_url"
                                    placeholder="picture_url"
                                    required
                                    value={this.state.picture_url}
                                    onChange={this.handlePictureURLChange}
                                />
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="manufacturer_id"
                                    id="manufacturer_id"
                                    required
                                    value={this.state.manufacturer_id}
                                    onChange={this.handleManufacturerIDChange}
                                >
                                    <option>Choose a Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer_id => {
                                        return (
                                            <option
                                                key={manufacturer_id.id}
                                                value={manufacturer_id.id}
                                            >
                                            {manufacturer_id.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default CreateVehicleModelForm;
